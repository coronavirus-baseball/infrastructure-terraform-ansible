provider "aws" {
  profile    = "coronavirus"
  region     = "us-west-2"
}

# VPC
resource "aws_vpc" "main" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name    = "coronavirus-main"
    Project = "coronavirus-baseball"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.main.id}"

  tags = {
    Name    = "coronavirus-main"
    Project = "coronavirus-baseball"
  }
}

resource "aws_route_table" "rtb" {
  vpc_id = "${aws_vpc.main.id}"
  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = "${aws_internet_gateway.gw.id}"
  }
  
  tags = {
    Name    = "coronavirus-main"
    Project = "coronavirus-baseball"
  }
}

resource "aws_subnet" "main" {
  vpc_id     = "${aws_vpc.main.id}"
  cidr_block = "10.0.1.0/24"

  tags = {
    Name    = "coronavirus-main"
    Project = "coronavirus-baseball"
  }
}

resource "aws_route_table_association" "rta_subnet_public" {
  subnet_id      = "${aws_subnet.main.id}"
  route_table_id = "${aws_route_table.rtb.id}"
}

# Security Group
resource "aws_security_group" "ec2-sg" {
  name        = "coronavirus-ec2-sg"
  description = "Allow TLS inbound traffic"
  vpc_id = "${aws_vpc.main.id}"

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = {
    Name    = "coronavirus-ec2-sg"
    Project = "coronavirus-baseball"
  }
}

resource "aws_security_group_rule" "allow_tls" {
  type              = "ingress"
  to_port           = 443
  protocol          = "-1"
  from_port         = 443
  security_group_id = "${aws_security_group.ec2-sg.id}"
  cidr_blocks       = ["${aws_vpc.main.cidr_block}"]
}

resource "aws_security_group_rule" "allow_http" {
  type              = "ingress"
  to_port           = 80
  protocol          = "-1"
  from_port         = 80
  security_group_id = "${aws_security_group.ec2-sg.id}"
  cidr_blocks       = ["${aws_vpc.main.cidr_block}"]
}

resource "aws_security_group_rule" "allow_ssh" {
  type              = "ingress"
  to_port           = 22
  protocol          = "-1"
  from_port         = 22
  security_group_id = "${aws_security_group.ec2-sg.id}"
  cidr_blocks       = ["${aws_vpc.main.cidr_block}"]
}

# EC2
resource "aws_iam_role" "ec2-role" {
  name = "coronavirus-ec2-role"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_role_policy" "ec2-policy" {
  name = "coronavirus-ec2-policy"
  role = "${aws_iam_role.ec2-role.id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "s3:GetAccessPoint",
                "s3:PutAccountPublicAccessBlock",
                "dynamodb:ListTables",
                "s3:ListAccessPoints",
                "dynamodb:ListBackups",
                "s3:ListJobs",
                "dynamodb:PurchaseReservedCapacityOfferings",
                "dynamodb:ListStreams",
                "dynamodb:ListContributorInsights",
                "dynamodb:DescribeReservedCapacityOfferings",
                "s3:GetAccountPublicAccessBlock",
                "dynamodb:ListGlobalTables",
                "s3:ListAllMyBuckets",
                "dynamodb:DescribeReservedCapacity",
                "s3:CreateJob",
                "s3:HeadBucket",
                "dynamodb:DescribeLimits"
            ],
            "Resource": "*"
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": [
                "s3:*",
                "dynamodb:*"
            ],
            "Resource": [
                "arn:aws:dynamodb:*:*:table/*/index/*",
                "arn:aws:dynamodb:*:306020647686:table/coronavirus_baseball_videos",
                "arn:aws:dynamodb:*:*:table/*/stream/*",
                "arn:aws:dynamodb:*:*:table/*/backup/*",
                "arn:aws:dynamodb::*:global-table/*",
                "arn:aws:s3:::coronavirus-baseball.com",
                "arn:aws:s3:*:*:accesspoint/*",
                "arn:aws:s3:::*/*",
                "arn:aws:s3:*:*:job/*"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_instance_profile" "ec2-profile" {
  name = "coronavirus-ec2-profile"
  role = "${aws_iam_role.ec2-role.name}"
}

data "aws_ami" "amazon-linux-2" {
 most_recent = true
 owners      = ["137112412989"] # amazon

 filter {
   name   = "name"
   values = ["amzn2-ami-hvm*"]
 }
}

resource "aws_instance" "web" {
  ami                    = "${data.aws_ami.amazon-linux-2.id}"
  instance_type          = "t2.micro"
  vpc_security_group_ids = ["${aws_security_group.ec2-sg.id}"]
  subnet_id              = "${aws_subnet.main.id}"
  iam_instance_profile   = "${aws_iam_instance_profile.ec2-profile.name}"
  key_name               = "coronavirus-ec2-key"

  tags = {
    Name    = "coronavirus-ec2-web"
    Project = "coronavirus-baseball"
  }
}

resource "aws_dynamodb_table" "videos-table" {
  name           = "coronavirus_baseball_videos"
  billing_mode   = "PAY_PER_REQUEST"
  hash_key       = "id"
  range_key      = "created_at"

  attribute {
    name = "id"
    type = "S"
  }

  attribute {
    name = "team"
    type = "S"
  }

  attribute {
    name = "season"
    type = "S"
  }

  attribute {
    name = "created_at"
    type = "S"
  }

  ttl {
    attribute_name = "TimeToExist"
    enabled        = false
  }

  global_secondary_index {
    name               = "team_index"
    hash_key           = "team"
    range_key          = "created_at"
    projection_type    = "INCLUDE"
    non_key_attributes = ["id", "season", "title", "description", "thumbnail"]
  }

  global_secondary_index {
    name               = "season_index"
    hash_key           = "season"
    range_key          = "created_at"
    projection_type    = "INCLUDE"
    non_key_attributes = ["id", "team", "title", "description", "thumbnail"]
  }

  tags = {
    Name    = "coronavirus-dynamodb-videos"
    Project = "coronavirus-baseball"
  }
}

# TODO: Lambda
# TODO: API Gateway

# Route53
resource "aws_route53_zone" "domain" {
  name = "coronavirus-baseball.com"
}

resource "aws_route53_record" "www" {
  zone_id = "${aws_route53_zone.domain.zone_id}"
  name    = "www.coronavirus-baseball.com"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.web.public_ip}"]
}

# S3
resource "aws_s3_bucket" "s3" {
  bucket = "coronavirus-baseball.com"
  acl    = "private"

  tags = {
    Name    = "coronavirus-baseball.com"
    Project = "coronavirus-baseball"
  }
}
